/*
Package utils provides generic utility structures, functions, and classes.
*/
package utils

import "gitlab.com/sethll/gocode/utils/pkg/gestfo"

var gestfoTestCases = []struct{
	description string
	inputFormat string
	inputValues gestfo.V
	expectedVal string
}{
	{
		description: "best test",
		inputFormat: "{wrod0} {wrod1} {tehe}",
		inputValues: gestfo.V{
			"wrod1": "there",
			"tehe": ":)",
			"wrod0": "hi",
		},
		expectedVal: "hi there :)",
	},
	{
		description: "hello world",
		inputFormat: "{h}, {w}!",
		inputValues: gestfo.V{
			"h": "Hello",
			"w": "World",
		},
		expectedVal: "Hello, World!",
	},
	{
		description: "weird",
		inputFormat: "",
		inputValues: gestfo.V{
			"t": "test",
		},
		expectedVal: "",
	},
	{
		description: "too many values",
		inputFormat: "{t} {m}",
		inputValues: gestfo.V{
			"t": "too",
			"m": "many",
			"v": "values",
		},
		expectedVal: "too many",
	},
	{
		description: "not enough values",
		inputFormat: "{n} {e} {v}",
		inputValues: gestfo.V{
			"n": "not",
			"e": "enough",
			"t": "time",
		},
		expectedVal: "not enough {v}",
	},
}