/*
Package utils provides generic utility structures, functions, and classes.
*/
package utils

import (
	"gitlab.com/sethll/gocode/utils/pkg/gestfo"
	"testing"
)

func TestGestfo(t *testing.T)  {
	for _, c := range gestfoTestCases {
		res := gestfo.G(c.inputFormat, c.inputValues)

		if res != c.expectedVal {
			t.Fatalf("FAIL: %s\nExpected '%s', got '%s'", c.description, c.expectedVal, res)
		}

		t.Logf("PASS: %q; format: '%s'; res: '%s'", c.description, c.inputFormat, res)
	}
}

func BenchmarkGestfo(b *testing.B) {
	for i := 0; i < b.N; i++ {

		for _, c := range gestfoTestCases {
			gestfo.G(c.inputFormat, c.inputValues)
		}

	}

}