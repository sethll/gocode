package main

import (
	"fmt"
	"gitlab.com/sethll/gocode/utils/pkg/gestfo"
)

func doGestfo() {
	vals := gestfo.V{
		"a": "apple",
		"b": "banana",
	}
	format := "Want an {a}? or a {b}?"

	str := gestfo.G(format, vals)
	fmt.Println(str)
}

func main() {
	doGestfo()
}
