/*
Package gestfo provides a GEneric STring FOrmatter function and its accompanying data type.
 */
package gestfo

/*
   Copyright 2020 Seth L (gitlab.com/sethll)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

import (
	"fmt"
	"strings"
)

type V map[string]interface{}

/*
G is short for gestfo, and gestfo is a GEneric STring FOrmatter. (The word 'gestfo' is, to my knowledge, gibberish.) Really only useful if you like explicitly naming your string formatter inputs, or need to rearrange your value specifications for some reason.

Use

	import "gitlab.com/sethll/gocode/utils"

	func main() {
		s := utils.G("{h}, {w}!", utils.V{"w": "World","h": "Hello"})
	}
*/
func G(format string, values V) (formatted string) {
	/*
		LICENSE CC BY-SA 4.0

		This work is licensed under a
		Creative Commons Attribution-ShareAlike 4.0 International License.
		<https://creativecommons.org/licenses/by-sa/4.0/>
	*/
	// (Due to modifying code from StackOverflow). Totally ripped off, Marty.
	// https://stackoverflow.com/a/40811635 <- can't improve on perfection?

	args, itIndex := make([]string, len(values)*2), 0

	for k, v := range values {
		args[itIndex] = "{" + k + "}"
		args[itIndex+1] = fmt.Sprint(v)
		itIndex += 2
	}

	formatted = strings.NewReplacer(args...).Replace(format)
	return
}
